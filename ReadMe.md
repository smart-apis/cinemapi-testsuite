## Cinemapi Testsuite

This repository contains black-box tests for Cinemapi.

## Usage

Run the tests against a local Cinemapi instance running on http://localhost:8080
```bash
npm test
```

Run the tests against the mock instance at http://screening-events.test
```bash
npm run test-mock
```

Run the tests against the production application at https://screening-events.herokuapp.com 
```bash
npm run test-prod
```
