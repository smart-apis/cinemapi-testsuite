import {clearCache, fetchAndMerge, fetchEntrypoint, operationAndMerge} from "./test-client";
import {frame} from "jsonld";
import fetch from 'node-fetch';
import moment from 'moment';

jest.setTimeout(30000);

describe('GIVEN a client fetched the screening event collection', () => {

  let facts;
  beforeAll(async () => {
    // given the API entrypoint and hydra documentation is fetched
    facts = await fetchEntrypoint();

    // and then the collection of screening events is fetched
    const screeningEventCollections = await frame(facts.all(), {
      "@context": {
        "hydra": "http://www.w3.org/ns/hydra/core#",
        "schema": "https://schema.org/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#"
      },
      "@type": "hydra:Collection",
      "hydra:manages": {
        "hydra:property": {
          "@id": "rdf:type"
        },
        "hydra:object": {
          "@id": "schema:ScreeningEvent"
        }
      }
    });
    const firstMatch = screeningEventCollections['@graph'][0];
    const guideUri = firstMatch['@id'];

    await fetchAndMerge(guideUri);
  });

  describe('AND it chose one event', function () {
    let aquamanEventUri;
    beforeAll(async () => {
      // the data graph is queried for screening events
      const screeningEventsFrame = {
        "@context": {
          "@vocab": "https://schema.org/"
        },
        "@type": "ScreeningEvent"
      };

      const screeningEventsMatches = await frame(facts.all(),
          screeningEventsFrame);
      const screeningEvents = screeningEventsMatches['@graph'];

      aquamanEventUri
          = screeningEvents.find(
          it => it.workPresented.name === 'Aquaman')["@id"];
      await fetchAndMerge(aquamanEventUri);
    });

    describe('AND picked an offer for that event', async () => {

      let aquamanEventOffer;
      beforeAll(async () => {
        const aquamanEventFrame = {
          "@context": {
            "@vocab": "https://schema.org/",
            "type": "@type"
          },
          "@id": aquamanEventUri,
          "@explicit": true,
          "offers": {}
        };
        const aquamanEventsMatches = await frame(facts.all(),
            aquamanEventFrame);
        aquamanEventOffer = aquamanEventsMatches["@graph"][0].offers;
      });

      describe('AND it looked up, where to order', () => {
        let createOrderOperation;
        beforeAll(async () => {
          const offerToOrderOperationFrame = {
            "@context": {
              "@vocab": "https://schema.org/",
              "hydra": "http://www.w3.org/ns/hydra/core#",
              "id": "@id",
              "type": "@type",
              "expects": {
                "@id": "hydra:expects",
                "@type": "@vocab"
              },
              "returns": {
                "@id": "hydra:returns",
                "@type": "@vocab"
              },
              "method": "hydra:method",
              "operationTarget": {
                "@reverse": "hydra:operation"
              }
            },

            "type": "CreateAction",
            "expects": "Offer",
            "returns": "Order",
            "operationTarget": {}
          };
          const operationMatches = await frame(facts.all(),
              offerToOrderOperationFrame);
          createOrderOperation = operationMatches["@graph"][0]
        });

        describe('WHEN it orders the offer', () => {

          let orderId;
          beforeAll(async () => {
            orderId = await operationAndMerge(createOrderOperation, {
              "@id": aquamanEventOffer["@id"]
            });
          });

          it('THEN it should get the id of the created order', () => {
            expect(orderId).toMatch(new RegExp('https?://.*/orders/[A-Za-z0-9\-]+$'));
          });

          it('AND it should have data about that order', async () => {
            const order = await facts.getResource(orderId, {
              "@vocab": "https://schema.org/",
              "id": "@id",
              "type": "@type",
            });
            expect(order.id).toEqual(orderId);
            expect(order.type).toEqual('Order');
            expect(order.acceptedOffer.id).toEqual(aquamanEventOffer["@id"]);
            expect(order.acceptedOffer.offeredItem.id).toEqual(aquamanEventUri);
            // might fail, if a minute transition occurs while running the test
            expect(order.orderDate).toEqual(
                moment().seconds(0).milliseconds(0).toISOString());
          });

          describe('WHEN it dereferences the order after deleting client-side knowledge', () => {
            let order;
            let facts;
            beforeAll(async () => {
              clearCache();
              facts = await fetchAndMerge(orderId);
              order = await facts.getResource(orderId,{
                "@vocab": "https://schema.org/",
                "hydra": "http://www.w3.org/ns/hydra/core#",
                "id": "@id",
                "type": "@type"
              });
            });

            it('THEN it should get the data from the server', async () => {
              expect(order.id).toEqual(orderId);
              expect(order.type).toEqual("Order");
              expect(order.acceptedOffer.id).toEqual(aquamanEventOffer["@id"]);
              expect(order.acceptedOffer.offeredItem.id).toEqual(aquamanEventUri);
            });

            it('AND the order should be part of the orders collection', async () => {
              const collectionId = createOrderOperation.operationTarget.id;
              await fetchAndMerge(collectionId);
              const collection = await facts.getResource(collectionId, {
                "id": "@id",
                "members": {
                  "@id": "http://www.w3.org/ns/hydra/core#member",
                  "@container": "@set"
                }
              });
              const memberIds = collection.members.map(it => it.id);
              expect(memberIds).toContain(orderId);
            });
          });
        });
      });
    });
  });


});