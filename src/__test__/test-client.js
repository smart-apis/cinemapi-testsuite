import fetch from 'node-fetch';
import { flatten, frame } from 'jsonld';

import createStore from 'hyperfact';

let facts;

const apiEntryPoint = process.env.API_ENTRYPOINT;

export const clearCache = () => {
  facts = createStore();
};

export const fetchEntrypoint = async () => {
  clearCache();
  // request entrypoint and api documentation
  const response = await fetch(`${apiEntryPoint}/`);
  const linkHeader = response.headers.get('Link');

  const vocabRegExp = new RegExp('<(.*)>; rel="http://www.w3.org/ns/hydra/core#apiDocumentation"');
  const vocabUri = linkHeader.match(vocabRegExp)[1];

  const apiDoc = await fetch(vocabUri).then(r => r.json());
  const entrypoint = await response.json();

  await facts.merge(entrypoint);
  return await facts.merge(apiDoc);
};

export const fetchAndMerge = async (path) => {
  const response = await fetch(path);
  const resource = await response.json();
  return await facts.merge(resource);
};

export const operationAndMerge = async (operation, body) => {
  const response = await fetch(operation.operationTarget.id, {
    method: operation.method,
    headers: {
      "Content-Type": "application/ld+json",
      "Accept": "application/ld+json"
    },
    body: JSON.stringify(body)
  });
  const resource = await response.json();
  await facts.merge(resource);
  return response.headers.get('Location')
};