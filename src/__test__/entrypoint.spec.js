import { frame } from 'jsonld';

import {fetchEntrypoint} from "./test-client";

jest.setTimeout(30000);

describe('Cinemapi entrypoint', () => {

  it('should provide screening event collection', async () => {

    // given a client wants to find a collection of ScreeningEvents
    const aCollectionThatManagesScreeningEvents = {
      "@context": {
        "hydra": "http://www.w3.org/ns/hydra/core#",
        "schema": "https://schema.org/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#"
      },
      "@type": "hydra:Collection",
      "hydra:manages": {
        "hydra:property": {
          "@id": "rdf:type"
        },
        "hydra:object": {
          "@id": "schema:ScreeningEvent"
        }
      }
    };

    // when the API entrypoint and hydra documentation is fetched
    const facts = await fetchEntrypoint();

    // and the data graph is queried for the facts of interest
    const framingResult = await frame(facts.all(), aCollectionThatManagesScreeningEvents);
    const firstMatch = framingResult['@graph'][0];

    // then the client knows that it can find the ScreeningEvent collection at /guide
    expect(firstMatch['@id']).toMatch(new RegExp('https?://.*/guide$'));
    expect(firstMatch['hydra:manages']['hydra:property']['@id']).toEqual('rdf:type');
    expect(firstMatch['hydra:manages']['hydra:object']['@id']).toEqual('schema:ScreeningEvent');

  });
});
