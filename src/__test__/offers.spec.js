import {frame} from 'jsonld';

import {fetchAndMerge, fetchEntrypoint} from "./test-client";

jest.setTimeout(30000);

describe('GIVEN a client fetched the screening event collection', () => {

  let facts;
  beforeAll(async () => {
    // given the API entrypoint and hydra documentation is fetched
    facts = await fetchEntrypoint();

    // and then the collection of screening events is fetched
    const screeningEventCollections = await frame(facts.all(), {
      "@context": {
        "hydra": "http://www.w3.org/ns/hydra/core#",
        "schema": "https://schema.org/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#"
      },
      "@type": "hydra:Collection",
      "hydra:manages": {
        "hydra:property": {
          "@id": "rdf:type"
        },
        "hydra:object": {
          "@id": "schema:ScreeningEvent"
        }
      }
    });
    const firstMatch = screeningEventCollections['@graph'][0];
    const guideUri = firstMatch['@id'];

    await fetchAndMerge(guideUri);
  });

  describe('WHEN it chose one event', function () {
    let aquamanEventUri;
    beforeEach(async () => {
      // the data graph is queried for screening events
      const screeningEventsFrame = {
        "@context": {
          "@vocab": "https://schema.org/"
        },
        "@type": "ScreeningEvent"
      };

      const screeningEventsMatches = await frame(facts.all(),
          screeningEventsFrame);
      const screeningEvents = screeningEventsMatches['@graph'];

      aquamanEventUri
          = screeningEvents.find(
          it => it.workPresented.name === 'Aquaman')["@id"];
      await fetchAndMerge(aquamanEventUri);
    });

    it('THEN it can see an offer for that event', async () => {
      const aquamanEventFrame = {
        "@context": {
          "@vocab": "https://schema.org/",
          "type": "@type"
        },
        "@id": aquamanEventUri,
        "@explicit": true,
        "offers": {}
      };
      const aquamanEventsMatches = await frame(facts.all(),
          aquamanEventFrame);
      const aquamanEventOffer = aquamanEventsMatches["@graph"][0].offers;

      expect(aquamanEventOffer.type).toEqual("Offer");
      expect(aquamanEventOffer.priceSpecification.price).toEqual(12);
      expect(aquamanEventOffer.priceSpecification.priceCurrency).toEqual("EUR");
    });

  });

  describe('WHEN it looks for a place to order an offer', () => {

    let createOrderOperation;
    beforeEach(async () => {
      const offerToOrderOperationFrame = {
        "@context": {
          "@vocab": "https://schema.org/",
          "hydra": "http://www.w3.org/ns/hydra/core#",
          "id": "@id",
          "type": "@type",
          "expects": {
            "@id": "hydra:expects",
            "@type": "@vocab"
          },
          "returns": {
            "@id": "hydra:returns",
            "@type": "@vocab"
          },
          "method": "hydra:method",
          "operationTarget": {
            "@reverse": "hydra:operation"
          }
        },

        "type": "CreateAction",
        "expects": "Offer",
        "returns": "Order",
        "operationTarget": {}
      };
      const operationMatches = await frame(facts.all(),
          offerToOrderOperationFrame);
      createOrderOperation = operationMatches["@graph"][0]
    });

    it('THEN it sees it has to POST the Offer to /orders to create an Order', () => {
      expect(createOrderOperation.type).toContain('hydra:Operation');
      expect(createOrderOperation.type).toContain('CreateAction');
      expect(createOrderOperation.method).toEqual('POST');
      expect(createOrderOperation.expects).toEqual('Offer');
      expect(createOrderOperation.returns).toEqual('Order');
      expect(createOrderOperation.operationTarget.id).toMatch(new RegExp('https?://.*/orders$'));
    });

  });

});
