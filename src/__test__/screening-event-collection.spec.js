import {frame} from 'jsonld';

import {fetchAndMerge, fetchEntrypoint} from "./test-client";

jest.setTimeout(30000);

describe('Fetching the screening event collection', () => {

  let facts;

  beforeAll(async () => {
    // given the API entrypoint and hydra documentation is fetched
    facts = await fetchEntrypoint();

    // and then the collection of screening events is fetched
    const screeningEventCollections = await frame(facts.all(), {
      "@context": {
        "hydra": "http://www.w3.org/ns/hydra/core#",
        "schema": "https://schema.org/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#"
      },
      "@type": "hydra:Collection",
      "hydra:manages": {
        "hydra:property": {
          "@id": "rdf:type"
        },
        "hydra:object": {
          "@id": "schema:ScreeningEvent"
        }
      }
    });
    const firstMatch = screeningEventCollections['@graph'][0];
    const guideUri = firstMatch['@id'];

    facts = await fetchAndMerge(guideUri);
  });

  it('can find all screening events', async () => {
    // the data graph is queried for screening events
    const screeningEventsFrame = {
      "@context": {
        "@vocab": "https://schema.org/"
      },
      "@type": "ScreeningEvent"
    };

    const screeningEventsMatches = await frame(facts.all(), screeningEventsFrame);
    const screeningEvents = screeningEventsMatches['@graph'];

    // then the client does find all the screening events
    expect(screeningEvents).toHaveLength(6);
    const firstEvent = screeningEvents[0];
    expect(firstEvent['@id']).toMatch(new RegExp('https?://.*/events/1-0$'));
    const secondEvent = screeningEvents[1];
    expect(secondEvent['@id']).toMatch(new RegExp('https?://.*/events/1-1$'));
    const thirdEvent = screeningEvents[2];
    expect(thirdEvent['@id']).toMatch(new RegExp('https?://.*/events/1-2$'));
    const fourthEvent = screeningEvents[3];
    expect(fourthEvent['@id']).toMatch(new RegExp('https?://.*/events/2-0$'));
    const fifthEvent = screeningEvents[4];
    expect(fifthEvent['@id']).toMatch(new RegExp('https?://.*/events/2-1$'));
    const lastEvent = screeningEvents[5];
    expect(lastEvent['@id']).toMatch(new RegExp('https?://.*/events/3$'));
  });

  it('can see the base info for an event', async () => {
    // the data graph is queried for screening events
    const screeningEventsFrame = {
      "@context": {
        "@vocab": "https://schema.org/"
      },
      "@type": "ScreeningEvent"
    };

    const screeningEventsMatches = await frame(facts.all(), screeningEventsFrame);
    const screeningEvents = screeningEventsMatches['@graph'];

    // then the client does find all the screening events
    const event = screeningEvents.find(
        it => it['@id'].match(new RegExp('https?://.*/events/1-0$')));

    expect(event.workPresented.name).toBe('Aquaman');
    expect(event.startDate).toBe('2019-01-21T20:00');
    expect(event.location.name).toBe('Codecentric Office-Kino');
  });

  it('all events are associated with the cinema', async () => {
    // the data graph is queried for the cinema
    const cinemaFrame = {
      "@context": {
        "@vocab": "https://schema.org/"
      },
      "@type": "MovieTheater"
    };

    const cinemaMatches = await frame(facts.all(), cinemaFrame);
    const cinema = cinemaMatches['@graph'][0];

    // then the cinema is found
    expect(cinema['@id']).toMatch(new RegExp('https?://.*/cinemas/cc$'));
    expect(cinema['@type']).toEqual('MovieTheater');

    // and it links to all the screening events
    let events = cinema['event'];
    expect(events).toHaveLength(6);
    expect(events[0]['@id']).toMatch(new RegExp('https?://.*/events/1-0$'));
    expect(events[1]['@id']).toMatch(new RegExp('https?://.*/events/1-1$'));
    expect(events[2]['@id']).toMatch(new RegExp('https?://.*/events/1-2$'));
    expect(events[3]['@id']).toMatch(new RegExp('https?://.*/events/2-0$'));
    expect(events[4]['@id']).toMatch(new RegExp('https?://.*/events/2-1$'));
    expect(events[5]['@id']).toMatch(new RegExp('https?://.*/events/3$'));
  });

  it('can see when a movie is presented', async () => {
    // when: the data graph is queried for movies
    const movieFrame = {
      "@context": {
        "@vocab": "https://schema.org/",
        "presentedAt": {
          "@reverse": "workPresented"
        }
      },
      "@type": "Movie",
      "presentedAt": {
        "@explicit": true,
      }
    };

    const movieMatches = await frame(facts.all(), movieFrame);
    const movies = movieMatches['@graph'];

    // then: it can find 3 movies
    expect(movies).toHaveLength(3);

    // and: see the screening events of e.g. Aquaman
    let aquaman = movies.find(it => it.name === 'Aquaman');
    expect(aquaman.presentedAt).toEqual([{
      "@id": "http://screening-events.test/events/1-0",
      "@type": "ScreeningEvent"
    }, {
      "@id": "http://screening-events.test/events/1-1",
      "@type": "ScreeningEvent"
    }, {
      "@id": "http://screening-events.test/events/1-2",
      "@type": "ScreeningEvent"
    }])
  });

});
